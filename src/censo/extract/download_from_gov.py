import os
import requests

import urllib3
print("urllib3", urllib3.__version__)

url = 'https://download.inep.gov.br/dados_abertos/microdados_censo_escolar_2021.zip'
file = url.split('/')[-1]
output_folder = 'data/censo/raw'
output_file = f'{output_folder}/{file}'

# Create folder if it does no exist
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

def get_filename(url):
    """
    Parses filename from given url
    """
    if url.find('/'):
        return url.rsplit('/', 1)[1]
    
# Download multiple files
def get_files(url_list, outdir, replace = True):
    for url in url_list:
        # Parse filename
        fname = get_filename(url)
        outfp = os.path.join(outdir, fname)
        # Download the file if it does not exist already
        if (not os.path.exists(outfp) or (replace)):
            print("Downloading", fname)
#             r = urllib.request.urlretrieve(url, outfp, headers={"User-Agent": "python-urllib"})
            # Make http request for remote file data
            data = requests.get(url)
            # Save file data to local copy
            with open(outfp, 'wb')as file:
                file.write(data.content)

if __name__ == '__main__':

    if os.path.abspath(os.curdir).endswith('extract'):
        os.chdir('../../../')

    #downloads file from url into output_folder
    print(f'Downloading {url} into {output_folder}')
    # get_files([url], output_folder)
    response = requests.get(url, verify=False, allow_redirects=True)
    if(response.status_code == 200):
        file = open(output_file, 'wb')
        file.write(response.content)
        file.close()
    else:
        print(f'Error downloading {url}')
        print(f'Response status code: {response.status_code}')

    #unzips file into output_folder
    print(f'Unzipping {output_file} into {output_folder}')
    os.system(f'unzip {output_file} -d {output_folder}')
    


