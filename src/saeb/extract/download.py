import os
import requests
from zipfile import ZipFile

if __name__ == '__main__':
  urls = [
    'https://download.inep.gov.br/microdados/microdados_saeb_2021_ensino_fundamental_e_medio.zip',
    'https://download.inep.gov.br/microdados/microdados_saeb_2021_educacao_infantil.zip'
  ]

  files = [url.split('/')[-1] for url in urls]
  output_folder = 'data/saeb/raw'
  output_files = [f'{output_folder}/{file}' for file in files ]

  # Create folder if it does no exist
  if not os.path.exists(output_folder):
    os.makedirs(output_folder)

  if os.path.abspath(os.curdir).endswith('extract'):
    os.chdir('../../../')

  for i in range(len(urls)):

    # Downloads file from url into output_folder
    print(f'Downloading {urls[i]} into {output_folder}')

    response = requests.get(urls[i], verify=False, allow_redirects=True)
    
    if(response.status_code == 200):
      file = open(output_files[i], 'wb')
      file.write(response.content)
      file.close()

    else:
      print(f'Error downloading {urls[i]}')
      print(f'Response status code: {response.status_code}')

    # Create folder for each file that was downloaded
    file_name = files[i].split('.')[0]
    dir_name = f'{output_folder}/{file_name}'
    os.makedirs(dir_name)

    # unzips file into dir_name
    print(f'Unzipping {output_files[i]} into {output_folder}')
    with ZipFile(output_files[i], 'r') as zFile:
      zFile.extractall(path=dir_name)
      zFile.close()

    # remove .zip file
    os.remove(output_files[i])