import pandas as pd
import os
import sys

# Lista de colunas que serão selecionadas
columns_to_select = ['MUNICIPIO_CANDIDATO', 'NU_NOTA_L', 'NU_NOTA_CN', 'NU_NOTA_CH', 'NU_NOTA_M', 'NU_NOTA_R', 'NU_NOTA_CANDIDATO']

# Carregando dados
data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/sisu/chamada_regular/raw/')
file_path = os.path.join(data_dir, 'chamada_regular_2021-1.csv')

try:
    chamada_regular_2021_1 = pd.read_csv(file_path, sep=';', low_memory=False, usecols=columns_to_select)
    print('Arquivo carregado com sucesso')
except FileNotFoundError:
    print('Arquivo não encontrado')
    sys.exit(1)

print('Iniciando transformação dos dados...')

df_grouped = chamada_regular_2021_1.groupby('MUNICIPIO_CANDIDATO').agg({
    'NU_NOTA_L': lambda x: x.str.replace(',', '.').astype(float).mean(),
    'NU_NOTA_CN': lambda x: x.str.replace(',', '.').astype(float).mean(),
    'NU_NOTA_CH': lambda x: x.str.replace(',', '.').astype(float).mean(),
    'NU_NOTA_M': lambda x: x.str.replace(',', '.').astype(float).mean(),
    'NU_NOTA_R': 'mean',
    'NU_NOTA_CANDIDATO': lambda x: x.str.replace(',', '.').astype(float).mean()
})

# Criar DataFrame com a coluna "MUNICIPIO_CANDIDATO"
municipio_column = pd.DataFrame({'MUNICIPIO_CANDIDATO': df_grouped.index})

# Mesclar DataFrames para incluir a coluna com o nome do município
df_final = pd.merge(municipio_column, df_grouped, on='MUNICIPIO_CANDIDATO')

# Salvar DataFrame em CSV
df_final.to_csv('src/sisu/desempenho_por_municipio.csv', sep=';', index=False, encoding='utf-8')

print('Processo concluído.')
