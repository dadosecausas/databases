#!/usr/bin/env python
# coding: utf-8

# In[4]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os
from os import listdir
from os.path import isfile, join

data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/esus/')
data_mun = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/municipios/')

# fazer parsing dos campos de data?
datas_a_processar = ['dataInicioSintomas']

# remover datas antes de março/2020 e futuras?
remover_data_invalidas = True

# ## Lê dados processados do eSUS-VE

# In[5]:


df_orig = pd.read_csv(data_dir + 'todos_registros_esus-ve.csv')

today = pd.to_datetime('today')
for d in datas_a_processar:
    novo_campo = d #+ '_parsed'
    df_orig[novo_campo] = pd.to_datetime(df_orig[d], errors='coerce').dt.date
    # remove datas anteriores à pandemia e no futuro
    if remover_data_invalidas: df_orig = df_orig[(df_orig[novo_campo] >= pd.to_datetime('2020-03-01')) & (df_orig[novo_campo] <= today)]

df_pop = pd.read_csv(data_mun + 'populacao_nome_codigo.csv')

df_pop = df_pop[df_pop['estimated_population_2019'] > 200000]

df_orig = df_orig.merge(df_pop, left_on='municipioIBGE', right_on= 'city_ibge_code')
df_orig.drop('city_ibge_code', axis=1, inplace=True)

mun_list = ['Rio Branco-AC', 'Maceió-AL', 'Macapá-AP', 'Manaus-AM',
       'Salvador-BA', 'Fortaleza-CE', 'Brasília-DF', 'Vitória-ES',
       'Goiânia-GO', 'São Luís-MA', 'Cuiabá-MT', 'Campo Grande-MS',
       'Belo Horizonte-MG', 'Curitiba-PR', 'João Pessoa-PB', 'Belém-PA',
       'Recife-PE', 'Teresina-PI', 'Rio de Janeiro-RJ', 'Natal-RN',
       'Porto Alegre-RS', 'Porto Velho-RO', 'Boa Vista-RR',
       'Florianópolis-SC', 'Aracaju-SE', 'São Paulo-SP', 'Palmas-TO']

df_orig_capitais = df_orig[df_orig['Name'].isin(mun_list)]


# In[12]:



df_orig.to_csv(data_dir + 'registros_cidades_maiores_esus-ve.csv', index=False)


# In[ ]:
df_orig_capitais.to_csv(data_dir + 'registros_capitais_esus-ve.csv', index=False)



