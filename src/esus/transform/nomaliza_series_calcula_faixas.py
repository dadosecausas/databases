#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os
from os import listdir
from os.path import isfile, join

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)

data_dir = os.path.join(os.path.dirname(os.path.abspath('')), '..\\..\\data\\esus\\')
data_mun = os.path.join(os.path.dirname(os.path.abspath('')), '..\\..\\data\\municipios\\')


# In[2]:


df_orig = pd.read_csv(data_dir + 'registros_capitais_esus-ve.csv', parse_dates=['dataInicioSintomas'])

df_orig


# In[3]:


# Limpa idades inválidas
df_orig.loc[df_orig['idade'] > 110, 'idade'] = np.nan

# retira positivos descartados (considera preenchimento errado)
df_orig = df_orig[~((df_orig['classificacaoFinal'] == 'Descartado') & (df_orig['resultadoTeste'] == 'Positivo'))]

# retira registros sem informação sobre conclusão
df_orig = df_orig[~((df_orig['classificacaoFinal'].isna()) & (df_orig['resultadoTeste'].isna()))]

# so pra ficar mais bonitinho
df_orig['classificacaoFinal'] = df_orig['classificacaoFinal'].fillna("Não preenchido")

confirmados = ['Confirmado Laboratorial',
       'Confirmado Clínico-Epidemiológico',
       'Confirmado Clínico-Imagem',
       'Confirmação Laboratorial', 'Confirmado por Critério Clínico',
       'Confirmação Clínico Epidemiológico',
       'Confirmado Clinico-Imagem',
       'Confirmado Clinico-Epidemiologico']

# descartados são considerados descartados
df_orig.loc[(df_orig['classificacaoFinal'] == 'Descartado'), 'Classificação'] = 'Descartado'

# negativos são considerados descartados (por enquanto)
df_orig.loc[(df_orig['resultadoTeste'] == 'Negativo'), 'Classificação'] = 'Descartado'

# negativos são considerados descartados (por enquanto)
df_orig.loc[(df_orig['classificacaoFinal'] == 'Não preenchido') & (df_orig['resultadoTeste'] == 'Negativo'), 'Classificação'] = 'Descartado'


# confirmados ou positivos sao considerados confirmados
df_orig.loc[(df_orig['classificacaoFinal'].isin(confirmados)) | (df_orig['resultadoTeste'] == 'Positivo'), 'Classificação'] = 'Confirmado'

df_orig = df_orig[~df_orig['Classificação'].isna()]

df_orig


# In[4]:


#considera apenas os casos positivos
df_orig_confirmados = df_orig.loc[df_orig['Classificação'] == 'Confirmado'].copy()

df_orig_confirmados


# In[5]:


pd.crosstab(df_orig_confirmados['classificacaoFinal'], df_orig_confirmados['resultadoTeste'])


# In[6]:


#cria os grupos separados por faixa etária
df_orig_confirmados['Grupo'] = pd.cut(df_orig_confirmados['idade'], [0, 12, 18, 25, 55, 110])

df_orig_confirmados


# In[7]:


df_confirmados_grupo = df_orig_confirmados.groupby(['municipioIBGE', 'dataInicioSintomas', 'Grupo'])[['Classificação']].count().rename(columns={'Classificação':'Positivos'})

df_confirmados_grupo = df_confirmados_grupo.unstack()

df_confirmados_grupo


# In[8]:


#usa o stack para realizar as operações de filtragem a partir do 100o caso

df_confirmados_grupo_stack = df_confirmados_grupo.fillna(0).stack().reset_index()

df_confirmados_grupo_stack


# In[9]:


#cria a coluna contagem, com a contagem acumulada dos casos num municipio

df_confirmados_grupo_stack['Contagem'] = df_confirmados_grupo_stack.groupby('municipioIBGE')['Positivos'].transform(pd.Series.cumsum)

df_confirmados_grupo_stack.head(3000)


# In[10]:


#remove as colunas com contagem menor que 100, ou seja, só considera dias a partir do 100o caso

df_confirmados_grupo_stack = df_confirmados_grupo_stack.loc[df_confirmados_grupo_stack.Contagem >= 100]

df_confirmados_grupo_stack.head(3000)


# In[11]:


#remove a coluna contagem e cria as porcentagens de casos por faixa etária

df_confirmados_grupo_stack = df_confirmados_grupo_stack.drop(columns="Contagem")

df_confirmados_grupo_stack = df_confirmados_grupo_stack.set_index(['municipioIBGE', 'dataInicioSintomas', 'Grupo'])

df_confirmados_grupo_stack = df_confirmados_grupo_stack.unstack()

df_confirmados_porcentagem = df_confirmados_grupo_stack.apply(lambda x: round(100*x/x.sum(), 3), axis=1).fillna(0)

df_confirmados_porcentagem = df_confirmados_porcentagem.rename(columns={'Positivos':'Porcentagem'})

df_confirmados_grupo_stack = df_confirmados_grupo_stack.join(df_confirmados_porcentagem)

df_confirmados_grupo_stack


# In[12]:


df_confirmados_grupo_stack = df_confirmados_grupo_stack.fillna(0).stack().reset_index()

df_confirmados_grupo_stack


# In[13]:


#cria os numeros dos dias da epidemia, iniciando no dia com o 100o caso

df_contagem = df_confirmados_grupo_stack.groupby(['municipioIBGE', 'dataInicioSintomas']).last()

df_contagem['Dias'] = 1

df_contagem['diasEpidemia'] = df_contagem.groupby(['municipioIBGE'])['Dias'].transform(pd.Series.cumsum)

df_contagem['diasEpidemia'] = df_contagem['diasEpidemia'] - 1

df_contagem = df_contagem.drop(columns=['Dias', 'Positivos', 'Grupo', 'Porcentagem'])

df_contagem


# In[14]:


#coloca cada "dia da epidemia" em sua respectiva data
df_confirmados_grupo_stack = df_confirmados_grupo_stack.set_index(['municipioIBGE', 'dataInicioSintomas'])
df_finalizado = df_confirmados_grupo_stack.join(df_contagem)

df_finalizado.head(3000)


# In[15]:


#coloca o nome do município na tabela

df_municipio = df_orig.groupby('municipioIBGE').last()['Name']

df_finalizado = df_finalizado.reset_index().set_index('municipioIBGE').join(df_municipio)

df_finalizado


# In[16]:


#seleciona apenas os primeiros 60 de cada municipio

df_primeiros = df_finalizado.groupby('municipioIBGE').apply(lambda df: df.loc[df['diasEpidemia'] < 60])

df_primeiros = df_primeiros.reset_index(drop=True)

df_primeiros


# In[17]:


#seleciona os ultimos 60 de cada municipio

def ultimos(df):
    ultimo_dia = df.tail(1)['diasEpidemia'].item()
    return df.loc[df.diasEpidemia > ultimo_dia - 60]

df_ultimos = df_finalizado.groupby('municipioIBGE').apply(ultimos)

df_ultimos = df_ultimos.reset_index(drop=True)

df_ultimos


# In[18]:


df_finalizado.to_csv(data_dir + 'series_normalizadas_faixas_esus-ve.csv', index=False)
df_primeiros.to_csv(data_dir + 'series_inicio_faixas_esus-ve.csv', index=False)
df_ultimos.to_csv(data_dir + 'series_avancadas_faixas_esus-ve.csv', index=False)


# In[ ]:




