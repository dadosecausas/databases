#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os
from os import listdir
from os.path import isfile, join

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)

data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/esus/')
data_mun = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/municipios/')


# ## Lê dados processados do eSUS-VE

# In[2]:


# df_orig = pd.read_csv(data_dir + 'todos_registros_esus-ve.csv')
# df_orig = pd.read_csv(data_dir + 'registros_capitais_esus-ve.csv')
df_orig = pd.read_csv(data_dir + 'registros_cidades_maiores_esus-ve.csv')

df_orig.loc[df_orig['idade'] > 110, 'idade'] = np.nan


df_orig = df_orig[(~df_orig['tipoTeste'].isna()) & (~df_orig['resultadoTeste'].isna())]

df_orig


# In[3]:


df_pop = pd.read_csv(data_mun + 'populacao_nome_codigo.csv').rename(columns={'city_ibge_code': 'municipioIBGE'}).set_index('municipioIBGE')

df_pop


# In[4]:


df_orig['tipoTeste'].isna().sum()


# In[8]:


df_orig.loc[(df_orig['tipoTeste'] != 'RT-PCR'), 'Teste'] = 'Outros'

df_orig.loc[df_orig['tipoTeste'] == 'RT-PCR', 'Teste'] = 'RT-PCR'

df_orig


# In[9]:


df_total = df_orig.groupby(['municipioIBGE', 'dataInicioSintomas', 'Teste'])[['tipoTeste']].count().rename(columns={'tipoTeste':'Total'})

df_total = df_total.unstack()

df_total[('Total', 'Todos')] = df_total.sum(axis=1)

df_total


# In[10]:


df_positivos = df_orig[df_orig['resultadoTeste'] == 'Positivo']

df_positivos


# In[11]:


df_total_positivos = df_positivos.groupby(['municipioIBGE', 'dataInicioSintomas', 'Teste'])[['tipoTeste']].count().rename(columns={'tipoTeste':'Positivos'})

df_total_positivos = df_total_positivos.unstack()

df_total_positivos[('Positivos', 'Todos')] = df_total_positivos.sum(axis=1)

df_total_positivos


# In[12]:


df_total_pos = df_total.join(df_total_positivos)

df_total_pos


# In[13]:


df_positividade = 100 * df_total_pos['Positivos'][['Outros', 'RT-PCR', 'Todos']]/df_total_pos['Total'][['Outros', 'RT-PCR', 'Todos']]

df_positividade


# In[14]:


df_positividade.columns = pd.MultiIndex.from_product([['Positividade'], df_positividade.columns])

df_positividade


# In[15]:


df_total_pos = df_total.join(df_positividade)

df_total_pos


# In[16]:


df_total_perM = df_total_pos['Total'].join(df_pop)

df_total_perM = df_total_perM[['Outros', 'RT-PCR', 'Todos']].apply(lambda x: 1000000* x/df_total_perM['estimated_population_2019'])

df_total_perM.columns = pd.MultiIndex.from_product([['Total por milhão'], df_total_perM.columns])

df_total_perM


# In[17]:


df_total_pos = df_total_pos.join(df_total_perM)

df_total_pos


# In[18]:


df_total_pos = df_total_pos.sort_index()

df_total_pos = df_total_pos.rolling(7).mean()

df_total_pos.columns = [' '.join(col).strip() for col in df_total_pos.columns.values]

df_total_pos = df_total_pos.join(df_pop['Name']).reset_index()

df_total_pos


# In[ ]:

df_total_pos.to_csv(data_dir + 'taxas_testagem_maiores_cidades_esus-ve.csv', index=False)

mun_list = ['Rio Branco-AC', 'Maceió-AL', 'Macapá-AP', 'Manaus-AM',
       'Salvador-BA', 'Fortaleza-CE', 'Brasília-DF', 'Vitória-ES',
       'Goiânia-GO', 'São Luís-MA', 'Cuiabá-MT', 'Campo Grande-MS',
       'Belo Horizonte-MG', 'Curitiba-PR', 'João Pessoa-PB', 'Belém-PA',
       'Recife-PE', 'Teresina-PI', 'Rio de Janeiro-RJ', 'Natal-RN',
       'Porto Alegre-RS', 'Porto Velho-RO', 'Boa Vista-RR',
       'Florianópolis-SC', 'Aracaju-SE', 'São Paulo-SP', 'Palmas-TO']

df_total_pos_capitais = df_total_pos[df_total_pos['Name'].isin(mun_list)]

df_total_pos_capitais.to_csv(data_dir + 'taxas_testagem_capitais_esus-ve.csv', index=False)


