#!/bin/bash

# get prompt parameter for the dataset to be executed
dataset=$1

# check if the parameter is empty
if [ -z "$dataset" ]
then
    echo "No dataset provided. Please, provide a dataset name as parameter."
    exit 1
fi

if [ "$dataset" = 'esus' ]
then
    echo "Transforming esus..."
    pipenv run python src/esus/transform/processa-todos-registros.py
    pipenv run python src/esus/transform/filtra_confirmados.py
    pipenv run python src/esus/transform/filtra_maiores_cidades.py
    pipenv run python src/esus/transform/calcula_taxas_testagem_capitais_e_maiores_cidades.py
    pipenv run python src/esus/transform/positivos_faixa_etaria_capitais.py
    exit 1
fi

if [ "$dataset" = 'sisu' ]
then
    echo "Transforming sisu..."
    pipenv run python src/sisu/transform/transform.py
    exit 1
fi

if [ "$dataset" = 'censo' ]
then
    echo "Transforming censo..."
    pipenv run python src/censo/Indicadores Economicos/extract.py
    exit 1
    echo "Transforming censo..."
    pipenv run python src/censo/download_from_gov.py
    exit 1
fi