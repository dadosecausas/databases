#!/bin/bash

#get prompt parameter for the dataset to be executed
dataset=$1

#check if the parameter is empty
if [ -z "$dataset" ]
then
    echo "No dataset provided. Please, provide a dataset name as parameter."
    exit 1
fi

#check if parameter equals to one of the datasets

if [ "$dataset" = 'esus' ]
then
    echo "Downloading esus..."
    pipenv run python src/esus/extract/download_from_esusve.py
    exit 1
fi

if [ "$dataset" = 'sisu_regular' ]
then
    echo "Downloading sisu_regular..."
    pipenv run python src/sisu/chamada_regular/extract/download.py
    exit 1
fi

if [ "$dataset" = 'sisu_espera' ]
then
    echo "Downloading esus..."
    pipenv run python src/sisu/lista_de_espera/extract/download.py
    exit 1
fi

if [ "$dataset" = 'censo' ]
then
    echo "Downloading censo..."
    pipenv run python src/censo/extract/download_from_gov.py
    exit 1
fi

if [ "$dataset" = 'saeb' ]
then
    echo "Downloading saeb..."
    pipenv run python src/saeb/extract/download.py
    exit 1
fi